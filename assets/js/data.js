let customersDoAndDonts = [
  {
    id: 0,
    dont: "michael eats his toast butter side down",
    do: "how offten and how much he buys from us",
    img: "1",
  },
  {
    id: 1,
    dont: "steve talks to his car bluey.",
    do: "if he opened our latest edm.",
    img: "2",
  },
  {
    id: 2,
    dont: "joanne likes jumping on the bed",
    do: "how she responded to our latest online offer",
    img: "3",
  },
];

let solutionData = [
  {
    id: 1,
    title: "improve marketing performance.",
    desc: "Better response, click throughs and conversion rates with better targeting of customers and increasing retention",
    img: "1",
  },
  {
    id: 2,
    title: "increase sales and margin",
    desc: "Cross campaign optimisation and real time predictive modeling identifies profitable opportunities.",
    img: "2",
  },
  {
    id: 3,
    title: "boost customer retention and satisfaction.",
    desc: "Identify and eliminate triggers for customer defection by segment, proactively share customer feedback and initiate service requests across channels/departments, and provide more accurate and relevant offers.",
    img: "3",
  },
];
