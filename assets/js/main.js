"use strict";

/**
 * Easy selector helper function
 */
const select = (el, all = false) => {
  el = el.trim();
  if (all) {
    return [...document.querySelectorAll(el)];
  } else {
    return document.querySelector(el);
  }
};
console.log("kokoda");
/**
 * Easy event listener function
 */
const on = (type, el, listener, all = false) => {
  let selectEl = select(el, all);
  if (selectEl) {
    if (all) {
      selectEl.forEach((e) => e.addEventListener(type, listener));
    } else {
      selectEl.addEventListener(type, listener);
    }
  }
};

/**
 * Easy on scroll event listener
 */
const onscroll = (el, listener) => {
  el.addEventListener("scroll", listener);
};

/**
 * Navbar links active state on scroll
 */
let navbarlinks = select("#navbar .scrollto", true);
const navbarlinksActive = () => {
  let position = window.scrollY + 200;
  navbarlinks.forEach((navbarlink) => {
    if (!navbarlink.hash) return;
    let section = select(navbarlink.hash);
    if (!section) return;
    if (
      position >= section.offsetTop &&
      position <= section.offsetTop + section.offsetHeight
    ) {
      navbarlink.classList.add("active");
    } else {
      navbarlink.classList.remove("active");
    }
  });
};
window.addEventListener("load", navbarlinksActive);
onscroll(document, navbarlinksActive);

/**
 * Scrolls to an element with header offset
 */
const scrollto = (el) => {
  let header = select("#header");
  let offset = header.offsetHeight;

  if (!header.classList.contains("header-scrolled")) {
    offset -= 16;
  }

  let elementPos = select(el).offsetTop;
  window.scrollTo({
    top: elementPos - offset,
    behavior: "smooth",
  });
};

/**
 * Header fixed top on scroll
 */
let selectHeader = select("#header");
if (selectHeader) {
  let headerOffset = selectHeader.offsetTop;
  let nextElement = selectHeader.nextElementSibling;
  const headerFixed = () => {
    if (headerOffset - window.scrollY <= 0) {
      selectHeader.classList.add("fixed-top");
      nextElement.classList.add("scrolled-offset");
    } else {
      selectHeader.classList.remove("fixed-top");
      nextElement.classList.remove("scrolled-offset");
    }
  };
  window.addEventListener("load", headerFixed);
  onscroll(document, headerFixed);
}
/**
 * Mobile nav toggle
 */
on("click", ".mobile-nav-toggle", function (e) {
  select("#navbar").classList.toggle("navbar-mobile");
  this.classList.toggle("bi-list");
  this.classList.toggle("bi-x");

  console.log("kva kva");
});

// customer carousel logic

let num = 0;

let dontText = document.querySelector(".dont_text");
let doText = document.querySelector(".do_text");
let cutImg = document.querySelector(".custo-img");
function slideOne(x) {
  if (x >= customersDoAndDonts.length) {
    x = 0;
  } else if (x < 0) {
    x = customersDoAndDonts.length;
  }
  dontText.innerText = customersDoAndDonts[x].dont;
  doText.innerHTML = customersDoAndDonts[x].do;
  cutImg.style.backgroundImage = `url('./assets/images/customer-carousel/${customersDoAndDonts[num].img}.png')`;
  console.log("slideone is alive");
}

function slide() {
  num++;
  if (num >= customersDoAndDonts.length) {
    num = 0;
  }
  dontText.textContent = customersDoAndDonts[num].dont;
  doText.textContent = customersDoAndDonts[num].do;
  cutImg.style.backgroundImage = `url('./assets/images/customer-carousel/${customersDoAndDonts[num].img}.png')`;

  console.log("slide is alive");
}
slideOne(0);

let roll = setInterval(slide, 9000);

let prevcust = document.querySelector(".prev-custom");
let nextcust = document.querySelectorAll(".next-custom");
prevcust.addEventListener("click", function () {
  clearInterval(roll);
  num--;
  slideOne(num);
  roll = setInterval(slide, 9000);
});
nextcust.forEach((e) =>
  e.addEventListener("click", function () {
    clearInterval(roll);
    num++;
    slideOne(num);
    roll = setInterval(slide, 9000);
  })
);
// solution slider

let loadOne = document.querySelector("#loadone");
let loadTwo = document.querySelector("#loadtwo");
let loadThree = document.querySelector("#loadthree");
let solutionTitle = document.querySelector("#solTextTitle");
let solutionDesc = document.querySelector("#solTextDesc");
let solutionImg = document.querySelector(".sol-img");
let solnumone = document.querySelector("#one");
let solnumtwo = document.querySelector("#two");
let solnumthree = document.querySelector("#three");
function turnRed(x) {
  x.classList.replace("gray-text", "red-text");
  console.log("add classes in here");
}
function turnOrdinary(x) {
  if (x.classList.contains("red-text"))
    x.classList.replace("red-text", "gray-text");
}
function getSlide(x) {
  solutionTitle.textContent = solutionData[x].title;
  solutionDesc.textContent = solutionData[x].desc;
  solutionImg.style.backgroundImage = `url('./assets/images/customer-carousel/${solutionData[x].img}.png')`;
}

function isChecked() {
  if (loadOne.checked) {
    console.log("one is alive");
    turnRed(solnumone);
    turnOrdinary(solnumtwo);
    turnOrdinary(solnumthree);
    getSlide(0);
  }
  if (loadTwo.checked) {
    console.log("two is alive");
    turnRed(solnumtwo);
    turnOrdinary(solnumthree);
    turnOrdinary(solnumone);
    getSlide(1);
  }
  if (loadThree.checked) {
    console.log("three is alive");
    turnRed(solnumthree);
    turnOrdinary(solnumtwo);
    turnOrdinary(solnumone);
    getSlide(2);
  }
}

loadOne.addEventListener("click", isChecked);
loadTwo.addEventListener("click", isChecked);
loadThree.addEventListener("click", isChecked);
